``` Pierre-Jean LE ROY ```
# Projet Docker avec FastAPI, MySQL et MongoDB

Ce projet utilise Docker pour orchestrer deux API développées avec FastAPI, l'une connectée à une base de données MySQL et l'autre à une base de données MongoDB.

## Installation

### Prérequis

Assurez-vous d'avoir Docker et Docker Compose installés sur votre système.

- [Docker Installation Guide](https://docs.docker.com/get-docker/)
- [Docker Compose Installation Guide](https://docs.docker.com/compose/install/)

### URL du gitlab
1. Gitlab Mongo DB : [https://gitlab.com/WiZZPJ/carpart_app](https://gitlab.com/WiZZPJ/carpart_app)
2. Gitlab MySQL : [https://gitlab.com/WiZZPJ/carpart_app_mysql](https://gitlab.com/WiZZPJ/carpart_app_mysql)

### URL du dockerhub
1. Docker Hub Mongo DB : [https://hub.docker.com/r/wizzpj/cartpart_app/tags](https://hub.docker.com/r/wizzpj/cartpart_app/tags) - Télécharger l'image main-mongo-api et main-mongodb
2. Docker Hub MySQL : [https://hub.docker.com/r/wizzpj/cartpart_app/tags](https://hub.docker.com/r/wizzpj/cartpart_app/tags) - Télécharger l'image main-api

Une fois télécharger récupérer le docker-compose.yml (dans le gitlab [carpart_app](https://gitlab.com/WiZZPJ/carpart_app)) et le placer dans un dossier.
Penser à garder le fichier .env aussi disponible.
Ensuite lancer la commande suivante : 
```bash
docker-compose up
```

